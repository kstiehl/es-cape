package escape

import (
	"context"
	"time"

	"github.com/olivere/elastic/v7"
)

const version = "0.0.1"

// IndexInfo contains all informations need to query, create, or delete an index
type IndexInfo interface {
	GetType() string
	GetMapping() string
	GetIndexName() string
}

// Document represents function that every elasticsearch document should implement
type Document interface {
	IndexInfo
	GetID() string
	GetJSONData() (string, error)
}

// Client represents a client
type Client struct {
	client *elastic.Client
}

// NewClient creates a new client with default settings
func NewClient(nodes []string) (*Client, error) {
	c, err := elastic.NewClient(
		elastic.SetURL(nodes...),
		elastic.SetSniff(true),
		elastic.SetHealthcheckInterval(15*time.Second),
		elastic.SetGzip(true),
	)
	if err != nil {
		return nil, err
	}
	return &Client{c}, err
}

// VerifyIndex makes sure index exists with the correct mapping
func (c *Client) VerifyIndex(info IndexInfo) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	_, err := c.client.CreateIndex(info.GetIndexName()).BodyString(info.GetMapping()).Do(ctx)
	cancel()
	if err != nil {
		return err
	}
	return nil
}

// IndexDocument takes a document and indexes it to elasticsearch
func (c *Client) IndexDocument(doc Document) error {
	json, err := doc.GetJSONData()
	if err != nil {
		return err
	}
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	_, err = c.client.Index().Index(doc.GetIndexName()).Id(doc.GetID()).Type(doc.GetType()).BodyString(json).Do(ctx)
	cancel()
	if err != nil {
		return err
	}
	return nil
}
