package escape

import (
	"testing"
	"os"
)

const (
	envTestNode = "TEST_ES_NODE"
)

func TestClientCreation(t *testing.T) {
	_, err := NewClient([]string{getEnv(t, envTestNode)});
	if err != nil {
		t.Errorf("Could not connect to elasticsearch:\n %s", err)
	}
}

func getEnv(t *testing.T, name string ) (env string) {
	var isPresent bool
	if env, isPresent = os.LookupEnv(name); !isPresent {
		t.Logf("%s was not set as an env variable", name)
		t.FailNow()
	}
	return
}